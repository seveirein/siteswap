import sys
import copy
from vanillaValidator import VanillaSiteswap

class NQSiteswap(VanillaSiteswap):
    def __init__(self, numberList, spaceTypeList=None):

        super(NQSiteswap, self).__init__(numberList)

        if spaceTypeList==None:
            spaceTypeList=[1]*len(numberList)

        self.spaceTypes= copy.copy(spaceTypeList)
        length = len(self.numbers)
       
        if length != len(spaceTypeList):
            raise ValueError('NQSiteswap space type list is not the same length as numberList')
    
        period = 0
        for index in range(length):
            if (spaceTypeList[index] != -1) and (spaceTypeList[index] != 1):
                raise ValueError('NQSiteswap has illegal spaceTypeList, must be 1s or -1s')
            period += spaceTypeList[index]

        self.period=period

    def __copy__(self):
        return NQSiteswap(self.numbers, self.spaceTypes)

    def numberToString(self, spaces, index):
        output = ""
        if self.spaceTypes[index] == -1:
            output += "!"

        output += super(NQSiteswap, self).numberToString(spaces, index)
        return output

    def getPeriod(self):
        return self.period

    def totalContributionAt(self, index):
        return self.numbers[index]*self.spaceTypes[index]

    def setTwoSequentialValues(self, positionIndex, pair):
        (indexA, indexB) = self.getTwoSequentialIndices(positionIndex)
        self.numbers[indexA] = pair[0]
        self.numbers[indexB] = pair[1]

    def getTwoSequentialSpaces(self, positionIndex):
        (indexA, indexB) = self.getTwoSequentialIndices(positionIndex)
        return (self.spaceTypes[indexA], self.spaceTypes[indexB])

    #Gotta override this.
    def getTwoSwappedValues(self, positionIndex):
        (a, b) = self.getTwoSequentialValues(positionIndex)
        (spaceA, spaceB) = self.getTwoSequentialSpaces(positionIndex)

        # The self.spaceTypes[nextIndex] multiplication can be proven to be valid
        # for the following cases:
        # a) exchanging two positive space numbers (well understood)
        # b) exchanging a positive and a negative space number (must behave this way
        #    to preserve average).
        # However, in the case of exchanging 2 negative space numbers, it is possible
        # that the exchange should be treated like a normal one.  Using the technique
        # below, in theory means that pure negative space siteswaps have to be written 
        # backwards.  IE 423 is valid, and 3{1}2{1}4{1} is valid but not 4{1}2{1}3{1}.
        #

        # To change the logic, you need to invert the signs when both nextIndex and 
        # postion index are have a spaceType of -1.
        #if (spaceA == -1) and (spaceB == -1):
        #    spaceA == 1
        #    spaceB == 1
        
        #Actually for now be conservative and don't do anything
        if (spaceA == -1) and (spaceB == -1):
            return (a, b)

        newA = b+1*spaceB
        newB = a-1*spaceA
        return (newA, newB)

    def cancel(self):
        isACancel = False
        cancelled = True
        while( cancelled ):
            cancelled = False
            for i in range(len(self.numbers)):
                (a,b) = self.getTwoSequentialValues(i)
                (aSpace,bSpace) = self.getTwoSequentialSpaces(i)
                #currently we allow cancellation both ways
                #IE in 311{-1}3 the 1s would cancel, as well as for
                #      31{-1}13
                #Really not sure this is correct.
                #Fairly confident the first cancellation form works, but
                #not sure about the second.
                if ( a == b ) and (aSpace != bSpace):
                    #be conservative and disallow second form
                    if aSpace==-1:
                        continue #Don't cancel

                    (aIndex, bIndex) = self.getTwoSequentialIndices(i)
                    if bIndex > aIndex:
                        del self.numbers[bIndex]
                        del self.spaceTypes[bIndex]
                        del self.numbers[aIndex]
                        del self.spaceTypes[aIndex]
                    else:
                        del self.numbers[aIndex]
                        del self.spaceTypes[aIndex]
                        del self.numbers[bIndex]
                        del self.spaceTypes[bIndex]
                    cancelled=True
                    isACancel = True
                    break
        return isACancel
                    

    #Augment this to swap the spaceTypes
    def swapWithNext(self, positionIndex):
        super(NQSiteswap, self).swapWithNext(positionIndex)
        (indexA, indexB) = self.getTwoSequentialIndices(positionIndex)
        temp = self.spaceTypes[indexA]
        self.spaceTypes[indexA] = self.spaceTypes[indexB]
        self.spaceTypes[indexB] = temp
        
    #Gotta augment validation step with 
    #cancellation.
    def validationStep(self, average, verbose=False):
        self.cancel() #period 0 siteswaps not allowed, so never should
                      #yield null siteswap
        return super(NQSiteswap,self).validationStep(average, verbose)

if __name__ ==  "__main__":
    #Run test on problem siteswap
    #siteswap=NQSiteswap([2,4,0], [-1,1,1])
    #siteswap.validate(verbose=True))
    #sys.exit(1)

    #Run some tests.
    lowThrow=0
    highThrow=5
    minDigits=7
    maxDigits=7

    numberOfThrows=(highThrow-lowThrow)+1

    for length in range(minDigits,maxDigits+1):        
        for pattern in range(pow(numberOfThrows*2,length)):
            numberList = []
            spaceTypeList = []
            tempPattern = pattern

            for digits in range(length):
                value=(tempPattern % (numberOfThrows*2))
                if value < numberOfThrows:
                    spaceTypeList.append(1)
                else:
                    value -= numberOfThrows
                    spaceTypeList.append(-1)
                value += lowThrow
                numberList.append(value)
                tempPattern //= numberOfThrows*2
        
            siteswap=NQSiteswap(numberList, spaceTypeList)
            if siteswap.period == 0:
                if siteswap.validate():
                    print("Error period zero siteswap %s validates when it shouldn't." % siteswap)
                    sys.exit(1)
                continue 
            if siteswap.cancel():
                #Was a cancel, not very interesting.
                continue
            if abs(siteswap.period) == len(siteswap.numbers):
                #Vanilla, or all negative, not very interesting.
                continue

            if abs(siteswap.period) == 1:
                if not siteswap.validate():
                    #print("Error siteswap %s doesn't validate when has period 1." % siteswap)
                    continue
                    #sys.exit(1)
                     
            #print interesting ones that validate for now                     
            
            if siteswap.validate():
                if abs(siteswap.period) >= 3:
                    print( "%s" % siteswap )




