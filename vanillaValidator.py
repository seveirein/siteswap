import copy 

class VanillaSiteswap(object):
    def __init__(self, numberList):
        self.numbers = copy.copy(numberList)

    def __copy__(self):
        return VanillaSiteswap(self.numbers)

    def numberToString(self, spaces, index):
        printValue=self.numbers[index]
        if not spaces and printValue >= 10:
           printValue=chr(printValue-10 + ord('a'))
               
        return "%s" % printValue
    
    def __unicode__(self):
        output = ""
        length = len(self.numbers)

        spaces=False
        for i in range(length):
            #if siteswap contains negative numbers or large throws
            #we want to use another format.
            if self.numbers[i] > (ord('z')-ord('a')+10):
                spaces=True
            if self.numbers[i]<0:
                spaces=True
         
       
        for i in range(length):
            output += self.numberToString( spaces, i)
            if spaces and (i < (length-1)):
                output += " "
        return output

    def __str__(self):
        return unicode(self).encode('utf-8')

    def getPeriod(self):
        return len(self.numbers) #Period is same as length of input list

    def totalContributionAt(self, index):
        return self.numbers[index]

    def calculateAverage(self):
        length = len(self.numbers)
        total = 0
        for i in range(length):
            total += self.totalContributionAt(i)
        remainder = total % self.getPeriod()
        if remainder != 0:
            raise ValueError('Siteswap has invalid average')
        average = total // self.getPeriod()
        return average
   
    def getTwoSequentialIndices(self, positionIndex):
        length = len(self.numbers)
        nextIndex = (positionIndex+1) % length
        return (positionIndex, nextIndex)

    def getTwoSequentialValues(self, positionIndex):
        (indexA, indexB) = self.getTwoSequentialIndices(positionIndex)
        return (self.numbers[indexA], self.numbers[indexB])
 
    def setTwoSequentialValues(self, positionIndex, pair):
        (indexA, indexB) = self.getTwoSequentialIndices(positionIndex)
        self.numbers[indexA] = pair[0]
        self.numbers[indexB] = pair[1]
 
    def getTwoSwappedValues(self, positionIndex):
        (a, b) = self.getTwoSequentialValues(positionIndex)
        newA = b+1
        newB = a-1
        return (newA, newB)

    def swapWithNext(self, positionIndex):
        pair = self.getTwoSwappedValues(positionIndex)
        self.setTwoSequentialValues(positionIndex, pair)

    def findMove(self, baseValue):
        bestDeviation=float("inf")
        solved=True

        for index in range(0, len(self.numbers)):
            (a,b)=self.getTwoSequentialValues(index)
            (newA,newB)=self.getTwoSwappedValues(index)

            deviation=max(abs(a-baseValue),abs(b-baseValue))
            if deviation != 0:
                solved=False
            
            newDeviation=max(abs(newA-baseValue),abs(newB-baseValue))
            
            if newDeviation < deviation:                
                return (False, index)

        return (solved, None)

    #return value is (finishedBoolean, validBoolean).  validBoolean is only
    #valid if finishedBoolean is true.
    def validationStep(self,average, verbose=False):
        (solved, moveIndex) = self.findMove(average)
        if(verbose):
            print("siteswap: %s" % self)
            print("Move %s %s" % (solved, moveIndex))
        if solved:
            return (True, True)
        if moveIndex == None:
            return (True, False) #No possible exchange, all decrementing numbers

        self.swapWithNext(moveIndex)
        return (False, None)

    
    def validate(self, verbose=False):
        if self.getPeriod() == 0:
            return False

        #make a copy of ourself because validation process normally
        #modifies internal data structures otherwise.
        workingCopy = copy.copy(self)
        lastDeviationsIndex = None
        try:
            average = self.calculateAverage()
        except ValueError,e:
            return False


        done=False
        while not done:
            (done,valid)=workingCopy.validationStep(average, verbose)
        
        return valid

if __name__ ==  "__main__":
    #Run some tests.
    lowThrow= 0
    highThrow=5
    minDigits=0 #must be at least 0
    maxDigits=6

    numberOfThrows=(highThrow-lowThrow)+1

    for length in range(minDigits,maxDigits+1):
        for pattern in range(pow(numberOfThrows,length)):
            numberList = []
            tempPattern = pattern

            for digits in range(length):
                numberList.append((tempPattern % numberOfThrows)+lowThrow)
                tempPattern //= numberOfThrows
           
            siteswap=VanillaSiteswap(numberList)
            print( "%s: %s" % (siteswap, siteswap.validate()) )






